<?php
/**
 * @file
 * ------------------------------------------------------------------------------------
 * Created by SAN Business Consultants
 * Designed and implemented by Frank Font (ffont@sanbusinessconsultants.com)
 * In collaboration with Andrew Casertano (acasertano@sanbusinessconsultants.com)
 * Open source enhancements to this module are welcome!  Contact SAN to share updates.
 *
 * Copyright 2014 SAN Business Consultants, a Maryland USA company (sanbusinessconsultants.com)
 *
 * Licensed under the GNU General Public License, Version 2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ------------------------------------------------------------------------------------
 *
 * This is a simple decision support engine module for Drupal.
 */



namespace simplerulesengine;

/**
 * This class is for database schema content
 *
 * @author Frank Font
 */
class DBSimpleRulesEngineSchema
{
    /**
     * Add all the schema content for the contra indication tables
     */
    public function addToSchema(&$schema)
    {
        $schema['simplerulesengine_rule'] = array(
          'description' => 'A simple rules engine rule',
          'fields' => array(
            'category_nm' => array(
              'type' => 'varchar',
              'length' => 20,
              'not null' => TRUE,
              'description' => 'Simply for grouping rules in a logic way',
            ),
            'rule_nm' => array(
              'type' => 'varchar',
              'length' => 40,
              'not null' => TRUE,
              'description' => 'Must be unique',
            ),
            'version' => array(
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
              'default' => 1,
              'description' => 'Increases each time change is saved',
            ),
            'summary_msg_tx' => array(
              'type' => 'varchar',
              'length' => 80,
              'not null' => TRUE,
              'description' => 'Static summary text to show the user when rule is triggered',
            ),
            'msg_tx' => array(
              'type' => 'varchar',
              'length' => 512,
              'not null' => TRUE,
              'description' => 'Text to show the user when rule is triggered',
            ),
            'explanation' => array(
              'type' => 'varchar',
              'length' => 2048,
              'not null' => TRUE,
              'description' => 'Explanation of the rule purpose',
            ),
            'req_ack_yn' => array(
              'type' => 'int',
              'not null' => TRUE,
              'default' => 1,
              'description' => 'If 1 then an acknowledgement is required',
            ),
            'trigger_crit' => array(
              'type' => 'varchar',
              'length' => 4096,
              'not null' => TRUE,
              'description' => 'The criteria that triggers the rule',
            ),
            'readonly_yn' => array(
              'type' => 'int',
              'not null' => TRUE,
              'default' => 0,
              'description' => 'If 1 then this rule record cannot be edited',
            ),
            'active_yn' => array(
              'type' => 'int',
              'not null' => TRUE,
              'default' => 1,
              'description' => 'If 0 then this rule is not active',
            ),
            'updated_dt' => array(
              'type' => 'datetime',
              'mysql_type' => 'datetime',  
              'not null' => TRUE,
              'description' => 'When this record was last updated',
            ),
            'created_dt' => array(
              'type' => 'datetime',
              'mysql_type' => 'datetime',  
              'not null' => FALSE,
              'description' => 'When this record was created',
            ),
          ),
          'primary key' => array('rule_nm'),
        );
        
        $schema['simplerulesengine_measure'] = array(
          'description' => 'A measure used by the simple rules engine',
          'fields' => array(
            'category_nm' => array(
              'type' => 'varchar',
              'length' => 20,
              'not null' => TRUE,
              'description' => 'Simply for grouping in a logical way',
            ),
            'measure_nm' => array(
              'type' => 'varchar',
              'length' => 40,
              'not null' => TRUE,
              'description' => 'The measure name',
            ),
            'version' => array(
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
              'default' => 1,
              'description' => 'The version number of this measure',
            ),
            'return_type' => array(
              'type' => 'varchar',
              'length' => 20,
              'not null' => TRUE,
              'description' => 'The returned data type',
            ),
            'purpose_tx' => array(
              'type' => 'varchar',
              'length' => 1024,
              'not null' => TRUE,
              'description' => 'Static text describing purpose of this measure',
            ),
            'criteria_tx' => array(
              'type' => 'varchar',
              'length' => 4096,
              'not null' => FALSE,
              'description' => 'The measure formula or INPUT if no formula',
            ),
            'readonly_yn' => array(
              'type' => 'int',
              'not null' => TRUE,
              'default' => 0,
              'description' => 'If 1 then this measure record should not be edited',
            ),
            'active_yn' => array(
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
              'default' => 1,
              'description' => 'If 0 then this measure is not available for use in new expressions',
            ),
            'updated_dt' => array(
              'type' => 'datetime',
              'mysql_type' => 'datetime',  
              'not null' => TRUE,
              'description' => 'When this record was updated',
            ),
            'created_dt' => array(
              'type' => 'datetime',
              'mysql_type' => 'datetime',  
              'not null' => TRUE,
              'description' => 'When this record was created',
            ),
          ),
          'primary key' => array('measure_nm'),
        );
    }    
}
    