<?php
/**
 * @file
 * ------------------------------------------------------------------------------------
 * Created by SAN Business Consultants
 * Designed and implemented by Frank Font (ffont@sanbusinessconsultants.com)
 * In collaboration with Andrew Casertano (acasertano@sanbusinessconsultants.com)
 * Open source enhancements to this module are welcome!  Contact SAN to share updates.
 *
 * Copyright 2014 SAN Business Consultants, a Maryland USA company (sanbusinessconsultants.com)
 *
 * Licensed under the GNU General Public License, Version 2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ------------------------------------------------------------------------------------
 *
 * This is a simple decision support engine module for Drupal.
 */

namespace simplerulesengine;

/**
 * Numeric operator class for parser tree
 *
 * @author Frank Font
 */
class TNONumeric extends \simplerulesengine\TNOperator
{
    public function __construct($oLeft,$operator_tx,$oRight)
    {
        parent::__construct($oLeft,$operator_tx,$oRight);
        $this->m_bNumeric = TRUE;
    }
    
    public function getValue($bVerboseDiagnostic = FALSE)
    {
        //Evaluate an expression.
        $left = $this->getLeftChild()->getValue();
        $right = $this->getRightChild()->getValue();
        if($this->m_operator_tx == '*')
        {
            $result = ($left * $right);
        } else if($this->m_operator_tx == '/') {
            $result = ($left / $right);
        } else if($this->m_operator_tx == '+') {
            $result = ($left + $right);
        } else if($this->m_operator_tx == '-') {
            $result = ($left - $right);
        } else {
            throw new \Exception('Cannot process evaluation for numeric operator [' . $this->m_operator_tx . '] on values [' . $left . '] and [' . $right . ']');
        }
        if($bVerboseDiagnostic)
        {
            drupal_set_message(t('Verbose numeric result: ' . $left . ' ' . $this->m_operator_tx . ' ' . $right));
        }
        return $result;
    }
}
