<?php
/**
 * ------------------------------------------------------------------------------------
 * Created by SAN Business Consultants
 * Designed and implemented by Frank Font (ffont@sanbusinessconsultants.com)
 * In collaboration with Andrew Casertano (acasertano@sanbusinessconsultants.com)
 * Open source enhancements to this module are welcome!  Contact SAN to share updates.
 *
 * Copyright 2014 SAN Business Consultants, a Maryland USA company (sanbusinessconsultants.com)
 *
 * Licensed under the GNU General Public License, Version 2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ------------------------------------------------------------------------------------
 *
 * This is a simple decision support engine module for Drupal.
 */


namespace simplerulesengine;

require_once ('TestUtility.inc');
require_once ('TestTokenizer.inc');
require_once ('TestMeasureExpression.inc');
require_once ('TestRuleExpression.inc');
require_once (dirname(__FILE__) . '/../MeasureExpressionParser.inc');

/**
 * Tests for the simplerulesengine parser
 *
 * @author Frank Font
 */
class TestAll
{
    public static function runAllTests()
    {
        $nFailed = 0;
        $nFailed += TestTokenizer::runAllTests();
        $nFailed += TestMeasureExpression::runAllTests();
        $nFailed += TestRuleExpression::runAllTests();
        return $nFailed;
    }    
    
    public static function runTokenizerTests()
    {
        $nTest = 0;
        $nFailedCount = 0;

        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(1 > 2)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        if($nExpectedTokenCount != count($tokens_array))
        {
            $nFailedCount++;
            $aTestDetail[] = ('Failed Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        } else {
            $aTestDetail[] = ('Okay Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        }

        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(10 > 20)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        if($nExpectedTokenCount != count($tokens_array) || $tokens_array[1] != 10 || $tokens_array[2] != '>' || $tokens_array[3] != 20)
        {
            $nFailedCount++;
            $aTestDetail[] = ('Failed Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        } else {
            $aTestDetail[] = ('Okay Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        }

        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(1 = 2)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        if($nExpectedTokenCount != count($tokens_array))
        {
            $nFailedCount++;
            $aTestDetail[] = ('Failed Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        } else {
            $aTestDetail[] = ('Okay Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        }

        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(1 <> 2)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        if($nExpectedTokenCount != count($tokens_array))
        {
            $nFailedCount++;
            $aTestDetail[] = ('Failed Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        } else {
            $aTestDetail[] = ('Okay Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        }

        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(1 <= 2)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        if($nExpectedTokenCount != count($tokens_array))
        {
            $nFailedCount++;
            $aTestDetail[] = ('Failed Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        } else {
            $aTestDetail[] = ('Okay Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        }

        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(1 >= 2)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        if($nExpectedTokenCount != count($tokens_array))
        {
            $nFailedCount++;
            $aTestDetail[] = ('Failed Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        } else {
            $aTestDetail[] = ('Okay Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        }

        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(VAR1 and VAR2)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        if($nExpectedTokenCount != count($tokens_array))
        {
            $nFailedCount++;
            $aTestDetail[] = ('Failed Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        } else {
            $aTestDetail[] = ('Okay Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        }
        
        $nTest++;
        $nExpectedTokenCount = 9;
        $expression = '((VAR1 or VAR2) and VAR3)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        if($nExpectedTokenCount != count($tokens_array))
        {
            $nFailedCount++;
            $aTestDetail[] = ('Failed Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        } else {
            $aTestDetail[] = ('Okay Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        }
        
        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(VAR1 or VAR2)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        if($nExpectedTokenCount != count($tokens_array))
        {
            $nFailedCount++;
            $aTestDetail[] = ('Failed Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        } else {
            $aTestDetail[] = ('Okay Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . count($tokens_array) . ' contents=' . print_r($tokens_array,TRUE) );
        }
        
        if($nFailedCount > 0)
        {
            drupal_set_message(t('Total token failed tests = ' . $nFailedCount . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'),'error');
        } else {
            drupal_set_message(t('All ' . $nTest . ' token tests succeeded!' . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'));
        }
        return $nFailedCount;
    }

    public static function runTreeTests()
    {
        TestAll::runTreeEvalTests();
        TestAll::runTreeNumericEvalTests();
        TestAll::runTreeCompleteTests();
    }

    public static function runTreeNumericEvalTests()
    {
        $aTestDetail = array();
        $nTest = 0;
        $nFailedCount = 0;
        $aNodes = array();  //Collect nodes as we build them for aggregate tests.
        
        $var_map = array();
        $var_map['MYVAR1'] = 55;
        $var_map['MYVAR2'] = 111;
        $var_map['MYVAR99'] = NULL;
        
        $nTest++;
        $nExpectedResult = 200;
        $root_node = TestAll::createNode_OCC(100, '+', 100);
        $bFailed = TestAll::runOneTreeNumericEvalTest($nTest, $nExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }
        
        $nTest++;
        $nExpectedResult = 45;
        $root_node = TestAll::createNode_OCC(200, '-', 155);
        $bFailed = TestAll::runOneTreeNumericEvalTest($nTest, $nExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $nExpectedResult = 20;
        $root_node = TestAll::createNode_OCC(4, '*', 5);
        $bFailed = TestAll::runOneTreeNumericEvalTest($nTest, $nExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $nExpectedResult = 40;
        $root_node = TestAll::createNode_OCC(200, '/', 5);
        $bFailed = TestAll::runOneTreeNumericEvalTest($nTest, $nExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        if($nFailedCount > 0)
        {
            drupal_set_message(t('Total failed tree numeric eval tests = ' . $nFailedCount . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'),'error');
        } else {
            drupal_set_message(t('All ' . $nTest . ' tree numeric eval tests succeeded!' . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'));
        }
        return $nFailedCount;
    }    
    
    public static function runTreeEvalTests()
    {
        $aTestDetail = array();
        $nTest = 0;
        $nFailedCount = 0;
        $aNodes = array();  //Collect nodes as we build them for aggregate tests.
        
        $nTest++;
        $bExpectedResult = FALSE;
        $root_node = TestAll::createNode_OCC(100, '>', 100);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }
        
        $nTest++;
        $bExpectedResult = FALSE;
        $root_node = TestAll::createNode_OCC(1, '>', 2);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }
        
        $nTest++;
        $bExpectedResult = TRUE;
        $root_node = TestAll::createNode_OCC(2, '>', 1);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }
        
        $nTest++;
        $bExpectedResult = FALSE;
        $root_node = TestAll::createNode_OCC(2, '<', 1);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $bExpectedResult = TRUE;
        $root_node = TestAll::createNode_OCC(1, '<', 2);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }
        
        $nTest++;
        $bExpectedResult = TRUE;
        $root_node = TestAll::createNode_OCC(1, '=', 1);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }
        
        $nTest++;
        $bExpectedResult = TRUE;
        $root_node = TestAll::createNode_OCC(1, '>=', 1);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $bExpectedResult = TRUE;
        $root_node = TestAll::createNode_OCC(115, '<=', 115);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $bExpectedResult = TRUE;
        $oLeftNode = TestAll::createNode_OCC(111, '<', 211);
        $oRightNode = TestAll::createNode_OCC(10, '<', 20);
        $root_node = TestAll::createNode_ONN($oLeftNode, 'and', $oRightNode);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }
        
        $nTest++;
        $bExpectedResult = FALSE;
        $oLeftNode = TestAll::createNode_OCC(111, '>', 222);
        $oRightNode = TestAll::createNode_OCC(1111, '>', 22222);
        $root_node = TestAll::createNode_ONN($oLeftNode, 'and', $oRightNode);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $bExpectedResult = FALSE;
        $oLeftNode = TestAll::createNode_OCC(1, '<', 2);
        $oRightNode = TestAll::createNode_OCC(1, '>', 2);
        $root_node = TestAll::createNode_ONN($oLeftNode, 'and', $oRightNode);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }
        
        $nTest++;
        $bExpectedResult = TRUE;
        $oLeftNode = TestAll::createNode_OCC(0, '<', 2);
        $oRightNode = TestAll::createNode_OCC(2, '>', 0);
        $root_node = TestAll::createNode_ONN($oLeftNode, 'and', $oRightNode);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $bExpectedResult = TRUE;
        $oLeftNode = TestAll::createNode_OCC(0, '<', 2);
        $oRightNode = TestAll::createNode_OCC(2, '>', 0);
        $root_node = TestAll::createNode_ONN($oLeftNode, 'or', $oRightNode);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $bExpectedResult = TRUE;
        $oLeftNode = TestAll::createNode_OCC(2, '<', 1);
        $oRightNode = TestAll::createNode_OCC(2, '>', 0);
        $root_node = TestAll::createNode_ONN($oLeftNode, 'or', $oRightNode);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $bExpectedResult = TRUE;
        $oLeftNode = TestAll::createNode_OCC(2,'>',1);
        $oRightNode = TestAll::createNode_OCC(2,'<',1);
        $root_node = TestAll::createNode_ONN($oLeftNode, 'or', $oRightNode);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $bExpectedResult = FALSE;
        $oLeftNode = TestAll::createNode_OCC(2,'<',1);
        $oRightNode = TestAll::createNode_OCC(2,'<',1);
        $root_node = TestAll::createNode_ONN($oLeftNode,'or',$oRightNode);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }
        
        $nTest++;
        $bExpectedResult = TRUE;
        $oLeftNode = TestAll::createNode_OCC(200,'<',100);
        $oRightNode = TestAll::createNode_OCC(200,'>',100);
        $root_node = TestAll::createNode_ONN($oLeftNode,'or',$oRightNode);
        $bFailed = TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }
        
        if($nFailedCount > 0)
        {
            drupal_set_message(t('Total failed tree eval tests = ' . $nFailedCount . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'),'error');
        } else {
            drupal_set_message(t('All ' . $nTest . ' tree eval tests succeeded!' . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'));
        }
        return $nFailedCount;
    }
    
    public static function runTreeCompleteTests()
    {
        $aTestDetail = array();
        $nTest = 0;
        $nFailedCount = 0;
        $aNodes = array();  //Collect nodes as we build them for aggregate tests.
        
        $var_map = array();
        $var_map['MYVAR1'] = 55;
        $var_map['MYVAR2'] = 111;
        $var_map['MYVAR99'] = NULL;
        
        $nTest++;
        $bExpectedResult = TRUE;
        $expression = '(78 > 56)';
        $bFailed = TestAll::runOneTreeCompleteTest($nTest, $bExpectedResult, $var_map, $expression, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }
        
        $nTest++;
        $bExpectedResult = FALSE;
        $expression = '((158 > 269) and (21 > 31))';
        $bFailed = TestAll::runOneTreeCompleteTest($nTest, $bExpectedResult, $var_map, $expression, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $bExpectedResult = TRUE;
        $expression = '((358 < 469) and (821 < 831))';
        $bFailed = TestAll::runOneTreeCompleteTest($nTest, $bExpectedResult, $var_map, $expression, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $bExpectedResult = TRUE;
        $expression = '(((458) < (569)) and ((921) < (931)))';
        $bFailed = TestAll::runOneTreeCompleteTest($nTest, $bExpectedResult, $var_map, $expression, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $bExpectedResult = TRUE;
        $expression = '(((458 < 569) and (921 < 931)) or (1 > 0))';
        $bFailed = TestAll::runOneTreeCompleteTest($nTest, $bExpectedResult, $var_map, $expression, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $bExpectedResult = TRUE;
        $expression = '((458 < 569) and (1 > 931)) or (1 > 0)';
        $bFailed = TestAll::runOneTreeCompleteTest($nTest, $bExpectedResult, $var_map, $expression, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $bExpectedResult = TRUE;
        $expression = '(MYVAR1 < 100) and (MYVAR2 > 100)';
        $bFailed = TestAll::runOneTreeCompleteTest($nTest, $bExpectedResult, $var_map, $expression, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $bExpectedResult = FALSE;
        $expression = '(MYVAR1 > 100) and (MYVAR2 > 100)';
        $bFailed = TestAll::runOneTreeCompleteTest($nTest, $bExpectedResult, $var_map, $expression, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        $nTest++;
        $bExpectedResult = NULL;
        $expression = '(MYVAR99 > 100) and (MYVAR1 > 100)';
        $bFailed = TestAll::runOneTreeCompleteTest($nTest, $bExpectedResult, $var_map, $expression, $aTestDetail, $aNodes);
        if($bFailed)
        {
            $nFailedCount++;
        }

        if($nFailedCount > 0)
        {
            drupal_set_message(t('Total tree complete failed tests = ' . $nFailedCount . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'),'error');
        } else {
            drupal_set_message(t('All ' . $nTest . ' tree complete tests succeeded!' . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'));
        }
        return $nFailedCount;
    }

    private static function runOneTreeCompleteTest($nTest,$bExpectedResult, $var_map, $expression,&$aTestDetail,&$aNodes)
    {
        $parserengine = new \simplerulesengine\MeasureExpressionParser($var_map);
        $root_node = $parserengine->parse($expression);
        return TestAll::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $aNodes);
    }
    
    private static function runOneTreeEvalTest($nTest,$bExpectedResult,$root_node,&$aTestDetail,&$aNodes)
    {
        $bFailed=FALSE;
        try
        {
            $bEvalResult = $root_node->getValue();
            if($bEvalResult === NULL)
            {
                $sEvalResultText = 'NULL';
            } else {
                $sEvalResultText = ($bEvalResult ? 'TRUE' : 'FALSE');
            }
            if($bExpectedResult === NULL)
            {
                $sExpectedResultTxt = 'NULL';
            } else {
                $sExpectedResultTxt = ($bExpectedResult ? 'TRUE' : 'FALSE');
            }
            if($bExpectedResult !== $bEvalResult && !($bExpectedResult === NULL && $bEvalResult === NULL))
            {
                $aTestDetail[] = ('Failed Test' . $nTest . ':' . $root_node . '=' . $sEvalResultText . ' expected ' . $sExpectedResultTxt );
                $bFailed=TRUE;
            } else {
                $aTestDetail[] = ('Okay Test' . $nTest . ':' . $root_node . '=' . $sEvalResultText . ' expected ' . $sExpectedResultTxt );
            }
            if($bEvalResult !== NULL)
            {
                $aNodes[$bEvalResult][] = $root_node;
            }
        } catch (\Exception $ex) {
            drupal_set_message(t('Caught error on Test' . $nTest . ' is ' . $ex->getMessage()));
            $bFailed=TRUE;
        }
        return $bFailed;
    }

    private static function runOneTreeNumericEvalTest($nTest,$nExpectedResult,$root_node,&$aTestDetail,&$aNodes)
    {
        $bFailed=FALSE;
        try
        {
            $nEvalResult = $root_node->getValue();
            if($nEvalResult === NULL)
            {
                $sEvalResultText = 'NULL';
            } else {
                $sEvalResultText = $nEvalResult;
            }
            if($nExpectedResult === NULL)
            {
                $sExpectedResultTxt = 'NULL';
            } else {
                $sExpectedResultTxt = $nExpectedResult;
            }
            if($nExpectedResult !== $nEvalResult && !($nExpectedResult === NULL && $nEvalResult === NULL))
            {
                $aTestDetail[] = ('Failed Test' . $nTest . ':' . $root_node . '=' . $sEvalResultText . ' expected ' . $sExpectedResultTxt );
                $bFailed=TRUE;
            } else {
                $aTestDetail[] = ('Okay Test' . $nTest . ':' . $root_node . '=' . $sEvalResultText . ' expected ' . $sExpectedResultTxt );
            }
            if($nEvalResult != NULL)
            {
                $aNodes[$nEvalResult][] = $root_node;
            }
        } catch (\Exception $ex) {
            drupal_set_message(t('Caught error on Test' . $nTest . ' is ' . $ex->getMessage()));
            $bFailed=TRUE;
        }
        return $bFailed;
    }
    
    private static function createNode_OCC($leftConstant, $operator_tx, $rightConstant)
    {
        $oLeft = new \simplerulesengine\TNConstant($leftConstant);
        $oRight = new \simplerulesengine\TNConstant($rightConstant);
        if(strpos('*/+-', $operator_tx) === FALSE)
        {
            $root_node = new \simplerulesengine\TNOBoolean($oLeft,$operator_tx,$oRight);
        } else {
            $root_node = new \simplerulesengine\TNONumeric($oLeft,$operator_tx,$oRight);
        }
        return $root_node;
    }
    
    private static function createNode_ONN($leftNode, $operator_tx, $rightNode)
    {
        $root_node = new \simplerulesengine\TNOBoolean($leftNode,$operator_tx,$rightNode);
        return $root_node;
    }
}
