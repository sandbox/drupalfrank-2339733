<?php
/**
 * ------------------------------------------------------------------------------------
 * Created by SAN Business Consultants
 * Designed and implemented by Frank Font (ffont@sanbusinessconsultants.com)
 * In collaboration with Andrew Casertano (acasertano@sanbusinessconsultants.com)
 * Open source enhancements to this module are welcome!  Contact SAN to share updates.
 *
 * Copyright 2014 SAN Business Consultants, a Maryland USA company (sanbusinessconsultants.com)
 *
 * Licensed under the GNU General Public License, Version 2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ------------------------------------------------------------------------------------
 *
 * This is a simple decision support engine module for Drupal.
 */


namespace simplerulesengine;

require_once (dirname(__FILE__) . '/../MeasureExpressionParser.inc');
require_once (dirname(__FILE__) . '/../ITreeNode.inc');
require_once (dirname(__FILE__) . '/../ATreeNode.inc');
require_once (dirname(__FILE__) . '/../TNConstant.inc');
require_once (dirname(__FILE__) . '/../TNOperator.inc');
require_once (dirname(__FILE__) . '/../TNOBoolean.inc');
require_once (dirname(__FILE__) . '/../TNONumeric.inc');
require_once (dirname(__FILE__) . '/../TNVariable.inc');

/**
 * Utilities for the parser tests
 *
 * @author Frank Font
 */
class TestUtility
{
    public static function runOneTreeCompleteTest($nTest,$bExpectedResult, $var_map, $expression,&$aTestDetail,&$nFailedCount,&$aNodes)
    {
        $bFailed=FALSE;
        try
        {
            $parserengine = new \simplerulesengine\MeasureExpressionParser($var_map);
            $root_node = $parserengine->parse($expression);
        } catch (\Exception $ex) {
            //Cannot assume that root is okay.
            $nFailedCount++;
            $aTestDetail[] = ('Failed Test' . $nTest . ': "' . $expression . '" = Caught error ' . $ex->getMessage() );
            $bFailed=TRUE;
            //die('LOOK NOW' . print_r($aTestDetail,TRUE));
        }
        if(!$bFailed)
        {
            $bFailed = TestUtility::runOneTreeEvalTest($nTest, $bExpectedResult, $root_node, $aTestDetail, $nFailedCount, $aNodes);
            if($bFailed)
            {
                //Add more debug info.
                $aTestDetail[count($aTestDetail)-1] .= '<br>NOTE Test'.$nTest . ' was for expression = "' . $expression . '"';
            }
        }
        return $bFailed;
    }

    
    public static function runOneMeasureCompilerTest($nTest, $bExpectSytaxError, $aExpectedVars, $var_map, $expression,&$aTestDetail,&$nFailedCount,&$aNodes)
    {
        TestUtility::runOneCompilerTestAnyType('Measure',$nTest, $bExpectSytaxError, $aExpectedVars, $var_map, $expression, $aTestDetail, $nFailedCount, $aNodes);
    }
    
    public static function runOneRuleCompilerTest($nTest, $bExpectSytaxError, $aExpectedVars, $var_map, $expression,&$aTestDetail,&$nFailedCount,&$aNodes)
    {
        TestUtility::runOneCompilerTestAnyType('Rule',$nTest, $bExpectSytaxError, $aExpectedVars, $var_map, $expression, $aTestDetail, $nFailedCount, $aNodes);
    }
    
    private static function runOneCompilerTestAnyType($sType,$nTest, $bExpectSytaxError, $aExpectedVars, $var_map, $expression,&$aTestDetail,&$nFailedCount,&$aNodes)
    {
        $bFailed=FALSE;
        $sResultMsg = NULL;
        $bCompilerErrors=FALSE;
        try
        {
            if($sType=='Measure')
            {
                $parserengine = new \simplerulesengine\MeasureExpressionParser($var_map);
            } else if($sType=='Rule') {
                $parserengine = new \simplerulesengine\RuleExpressionParser($var_map);
            } else {
                throw new \Exception('Unrecognized expression declared as type [' . $sType . ']');
            }
            $results_array = $parserengine->compile($expression);
            $bCompilerErrors = $results_array['haserrors'];
            if(!$bExpectSytaxError && $bCompilerErrors)
            {
                $sDetails = implode('<li>',$results_array['errors']);
                $sResultMsg = ('Failed Compiler Test' . $nTest . ': "' . $expression . '"<ol><li>' . $sDetails  .'</ol>');
                $bFailed=TRUE;
            }
        } catch (\Exception $ex) {
            if($bExpectSytaxError)
            {
                $sResultMsg = ('Okay Test' . $nTest . ':[' . $expression . '] has expected parser error; details->' . $ex->getMessage());
            } else {
                $sResultMsg = ('Failed Compiler Test' . $nTest . ': "' . $expression . '" = Caught error ' . $ex->getMessage() );
                $bFailed=TRUE;
            }
        }
        if(!$bExpectSytaxError && !$bFailed)
        {
            $aDepVars = $results_array['dependencies'];
            $aMissing = array();
            foreach($aDepVars as $var_name)
            {
                if(!in_array($var_name, $aExpectedVars))
                {
                    $aMissing[$var_name] = 'Invalid ' . $var_name;
                }
            }
            foreach($aExpectedVars as $var_name)
            {
                if(!in_array($var_name, $aDepVars))
                {
                    $aMissing[$var_name] = 'Expected ' . $var_name;
                }
            }
            if(count($aMissing) > 0)
            {
                $bFailed=TRUE;
                $sResultMsg = ('Failed Test' . $nTest . ':[' . $expression . '] has parsing issue for these '.count($aMissing).' variable names ' . print_r($aMissing,TRUE));
            } else {
                $sResultMsg = ('Okay Test' . $nTest . ':[' . $expression . '] has ' . count($aDepVars) . ' vars expected ' . count($aExpectedVars) . ' vars');
            }
        }
        if($bFailed)
        {
            $nFailedCount++;
            if($sResultMsg == NULL)
            {
                $sResultMsg = ('Failed Test' . $nTest . ':[' . $expression . '] compiler result = '. print_r($results_array,TRUE));
            }
        }
        if($sResultMsg == NULL)
        {
            $tf1 = $bExpectSytaxError ? 'Compiler Error' : 'Compiler Success';
            $tf2 = $bCompilerErrors ? 'Compiler Error' : 'Compiler Success';
            $sResultMsg = ('Okay Test' . $nTest . ':[' . $expression . '] expected "'.$tf1. '" and got "'.$tf2.'"');
        }
        $aTestDetail[] = $sResultMsg;        
        return $bFailed;
    }

    public static function runOneTreeEvalTest($nTest,$bExpectedResult,$root_node,&$aTestDetail,&$nFailedCount,&$aNodes)
    {
        $bFailed=FALSE;
        try
        {
            $bEvalResult = $root_node->getValue();
            if($bEvalResult === NULL)
            {
                $sEvalResultText = 'NULL';
            } else {
                $sEvalResultText = ($bEvalResult ? 'TRUE' : 'FALSE');
            }
            if($bExpectedResult === NULL)
            {
                $sExpectedResultTxt = 'NULL';
            } else {
                $sExpectedResultTxt = ($bExpectedResult ? 'TRUE' : 'FALSE');
            }
            if($bExpectedResult !== $bEvalResult && !($bExpectedResult === NULL && $bEvalResult === NULL))
            {
                $aTestDetail[] = ('Failed Test' . $nTest . ':' . $root_node . '=' . $sEvalResultText . ' expected ' . $sExpectedResultTxt );
                $bFailed=TRUE;
            } else {
                $aTestDetail[] = ('Okay Test' . $nTest . ':' . $root_node . '=' . $sEvalResultText . ' expected ' . $sExpectedResultTxt );
            }
            if($bEvalResult !== NULL)
            {
                $aNodes[$bEvalResult][] = $root_node;
            }
        } catch (\Exception $ex) {
            $aTestDetail[] = ('Failed Test' . $nTest . ':' . $root_node . '=' . 'Caught error ' . $ex->getMessage() );
            $bFailed=TRUE;
        }
        if($bFailed)
        {
            $nFailedCount++;
        }
        return $bFailed;
    }

    public static function runOneTreeNumericEvalTest($nTest,$nExpectedResult,$root_node,&$aTestDetail,&$nFailedCount,&$aNodes)
    {
        $bFailed=FALSE;
        try
        {
            $nEvalResult = $root_node->getValue();
            if($nEvalResult === NULL)
            {
                $sEvalResultText = 'NULL';
            } else {
                $sEvalResultText = $nEvalResult;
            }
            if($nExpectedResult === NULL)
            {
                $sExpectedResultTxt = 'NULL';
            } else {
                $sExpectedResultTxt = $nExpectedResult;
            }
            if($nExpectedResult !== $nEvalResult && !($nExpectedResult === NULL && $nEvalResult === NULL))
            {
                $aTestDetail[] = ('Failed Test' . $nTest . ':' . $root_node . '=' . $sEvalResultText . ' expected ' . $sExpectedResultTxt );
                $bFailed=TRUE;
            } else {
                $aTestDetail[] = ('Okay Test' . $nTest . ':' . $root_node . '=' . $sEvalResultText . ' expected ' . $sExpectedResultTxt );
            }
            if($nEvalResult != NULL)
            {
                $aNodes[$nEvalResult][] = $root_node;
            }
        } catch (\Exception $ex) {
            $aTestDetail[] = ('Failed Test' . $nTest . ':' . $root_node . '=' . 'Caught error ' . $ex->getMessage() );
            $bFailed=TRUE;
        }
        if($bFailed)
        {
            $nFailedCount++;
        }
        return $bFailed;
    }
    
    public static function runOneTreeTextEvalTest($nTest,$bExpectedResult,$root_node,&$aTestDetail,&$nFailedCount,&$aNodes)
    {
        $bFailed=FALSE;
        try
        {
            $bEvalResult = $root_node->getValue();
            if($bEvalResult === NULL)
            {
                $sEvalResultText = 'NULL';
            } else {
                $sEvalResultText = ($bEvalResult ? 'TRUE' : 'FALSE');
            }
            if($bExpectedResult === NULL)
            {
                $sExpectedResultTxt = 'NULL';
            } else {
                $sExpectedResultTxt = ($bExpectedResult ? 'TRUE' : 'FALSE');
            }
            if($bExpectedResult !== $bEvalResult && !($bExpectedResult === NULL && $bEvalResult === NULL))
            {
                $aTestDetail[] = ('Failed Test' . $nTest . ':' . $root_node . '=' . $sEvalResultText . ' expected ' . $sExpectedResultTxt );
                $bFailed=TRUE;
            } else {
                $aTestDetail[] = ('Okay Test' . $nTest . ':' . $root_node . '=' . $sEvalResultText . ' expected ' . $sExpectedResultTxt );
            }
            if($bEvalResult != NULL)
            {
                $aNodes[$bEvalResult][] = $root_node;
            }
        } catch (\Exception $ex) {
            $aTestDetail[] = ('Failed Test' . $nTest . ':' . $root_node . '=' . 'Caught error ' . $ex->getMessage() );
            $bFailed=TRUE;
        }
        if($bFailed)
        {
            $nFailedCount++;
        }
        return $bFailed;
    }

    public static function createNode_OCC($leftConstant, $operator_tx, $rightConstant)
    {
        $oLeft = new \simplerulesengine\TNConstant($leftConstant);
        $oRight = new \simplerulesengine\TNConstant($rightConstant);
        if(strpos('*/+-', $operator_tx) === FALSE)
        {
            $root_node = new \simplerulesengine\TNOBoolean($oLeft,$operator_tx,$oRight);
        } else {
            $root_node = new \simplerulesengine\TNONumeric($oLeft,$operator_tx,$oRight);
        }
        return $root_node;
    }
    
    public static function createNode_OTT($leftText, $operator_tx, $rightText)
    {
        $oLeft = new \simplerulesengine\TNText($leftText);
        $oRight = new \simplerulesengine\TNText($rightText);
        $root_node = new \simplerulesengine\TNOBoolean($oLeft,$operator_tx,$oRight);
        return $root_node;
    }

    public static function createNode_OVV($leftVarname, $operator_tx, $rightVarname, $var_map)
    {
        $oLeft = new \simplerulesengine\TNVariable($leftVarname,$var_map);
        $oRight = new \simplerulesengine\TNVariable($rightVarname,$var_map);
        return TestUtility::createNode_ONN($oLeft,$operator_tx,$oRight);
    }

    public static function createNode_OVT($leftVarname, $operator_tx, $rightText, $var_map)
    {
        $oLeft = new \simplerulesengine\TNVariable($leftVarname,$var_map);
        $oRight = new \simplerulesengine\TNText($rightText);
        return TestUtility::createNode_ONN($oLeft,$operator_tx,$oRight);
    }

    public static function createNode_OTV($leftText, $operator_tx, $rightVarname, $var_map)
    {
        $oLeft = new \simplerulesengine\TNText($leftText);
        $oRight = new \simplerulesengine\TNVariable($rightVarname,$var_map);
        return TestUtility::createNode_ONN($oLeft,$operator_tx,$oRight);
    }

    public static function createNode_ONN($leftNode, $operator_tx, $rightNode)
    {
        $root_node = new \simplerulesengine\TNOBoolean($leftNode,$operator_tx,$rightNode);
        return $root_node;
    }
}
