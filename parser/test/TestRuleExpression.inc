<?php
/**
 * ------------------------------------------------------------------------------------
 * Created by SAN Business Consultants
 * Designed and implemented by Frank Font (ffont@sanbusinessconsultants.com)
 * In collaboration with Andrew Casertano (acasertano@sanbusinessconsultants.com)
 * Open source enhancements to this module are welcome!  Contact SAN to share updates.
 *
 * Copyright 2014 SAN Business Consultants, a Maryland USA company (sanbusinessconsultants.com)
 *
 * Licensed under the GNU General Public License, Version 2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ------------------------------------------------------------------------------------
 *
 * This is a simple decision support engine module for Drupal.
 */


namespace simplerulesengine;

require_once (dirname(__FILE__) . '/../RuleExpressionParser.inc');

/**
 * Test the rule expression processing
 *
 * @author Frank Font
 */
class TestRuleExpression
{
    private $m_expression = NULL;
    private $m_root_node = NULL;

    public static function runAllTests()
    {
        $failedCount = 0;
        $failedCount += TestRuleExpression::runBadSyntaxCompilerTests();
        $failedCount += TestRuleExpression::runGoodSyntaxCompilerTests();
        if($failedCount == 0)
        {
            drupal_set_message(t('Found ZERO errors in Rule tests!'));
        }
        return $failedCount;
    }
    
    public static function runBadSyntaxCompilerTests()
    {
        $sTestTitle = 'Bad Syntax Expression Compiler';
        
        $aTestDetail = array();
        $nTest = 0;
        $nFailedCount = 0;
        $aNodes = array();  //Collect nodes as we build them for aggregate tests.
        
        $var_map = array();
        $var_map['MYVAR1'] = 55;
        $var_map['MYVAR2'] = 111;
        $var_map['MYVAR99'] = NULL;
        
        $nTest++;
        $bExpectSytaxError=TRUE;
        $aExpectedVars = array('MYVAR1','MYVAR2');
        $expression = '(MYVAR1 > 100) or not(MYVAR2 > 100)';
        TestUtility::runOneRuleCompilerTest($nTest, $bExpectSytaxError, $aExpectedVars, $var_map, $expression, $aTestDetail, $nFailedCount, $aNodes);

        $nTest++;
        $bExpectSytaxError=TRUE;
        $aExpectedVars = array();
        $expression = 'this is not a real formula';
        TestUtility::runOneRuleCompilerTest($nTest, $bExpectSytaxError, $aExpectedVars, $var_map, $expression, $aTestDetail, $nFailedCount, $aNodes);

        $nTest++;
        $bExpectSytaxError=TRUE;
        $aExpectedVars = array();
        $expression = '(1 + 2';
        TestUtility::runOneRuleCompilerTest($nTest, $bExpectSytaxError, $aExpectedVars, $var_map, $expression, $aTestDetail, $nFailedCount, $aNodes);

        $nTest++;
        $bExpectSytaxError=TRUE;
        $aExpectedVars = array();
        $expression = '1 + 2)';
        TestUtility::runOneRuleCompilerTest($nTest, $bExpectSytaxError, $aExpectedVars, $var_map, $expression, $aTestDetail, $nFailedCount, $aNodes);

        $nTest++;
        $bExpectSytaxError=TRUE;
        $aExpectedVars = array('MYVAR1');
        $expression = 'MYVAR1';
        TestUtility::runOneRuleCompilerTest($nTest, $bExpectSytaxError, $aExpectedVars, $var_map, $expression, $aTestDetail, $nFailedCount, $aNodes);

        $nTest++;
        $bExpectSytaxError=TRUE;
        $aExpectedVars = array('MYVAR1');
        $expression = 'AllFlagsFakeFunc(MYVAR1)';
        TestUtility::runOneRuleCompilerTest($nTest, $bExpectSytaxError, $aExpectedVars, $var_map, $expression, $aTestDetail, $nFailedCount, $aNodes);

        if($nFailedCount > 0)
        {
            drupal_set_message(t('Total '.$sTestTitle.' failed tests = ' . $nFailedCount . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'),'error');
        } else {
            drupal_set_message(t('All ' . $nTest . ' '.$sTestTitle.' tests succeeded!' . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'));
        }
        return $nFailedCount;
        
    }    
    
    public static function runGoodSyntaxCompilerTests()
    {
        $sTestTitle = 'Good Syntax Expression Compiler';
        
        $aTestDetail = array();
        $nTest = 0;
        $nFailedCount = 0;
        $aNodes = array();  //Collect nodes as we build them for aggregate tests.
        
        TestRuleExpression::runStandardCompilerTestsForOneFunction($nTest,'AnyFlagTrue', $aTestDetail, $nFailedCount, $aNodes);
        TestRuleExpression::runStandardCompilerTestsForOneFunction($nTest,'AllFlagsTrue', $aTestDetail, $nFailedCount, $aNodes);
        TestRuleExpression::runStandardCompilerTestsForOneFunction($nTest,'AllFlagsFalse', $aTestDetail, $nFailedCount, $aNodes);
        TestRuleExpression::runStandardCompilerTestsForOneFunction($nTest,'AllFlagsNull', $aTestDetail, $nFailedCount, $aNodes);
        
        $aFunctionNames = array('AllFlagsNull');
        TestRuleExpression::runStandardCompilerTestsForManyFunctions($nTest,$aFunctionNames, $aTestDetail, $nFailedCount, $aNodes);

        $aFunctionNames = array('AnyFlagTrue','AllFlagsNull');
        TestRuleExpression::runStandardCompilerTestsForManyFunctions($nTest,$aFunctionNames, $aTestDetail, $nFailedCount, $aNodes);

        $aFunctionNames = array('AnyFlagTrue','AllFlagsTrue','AllFlagsFalse');
        TestRuleExpression::runStandardCompilerTestsForManyFunctions($nTest,$aFunctionNames, $aTestDetail, $nFailedCount, $aNodes);

        $aFunctionNames = array('AnyFlagTrue','AllFlagsTrue','AllFlagsFalse','AllFlagsNull');
        TestRuleExpression::runStandardCompilerTestsForManyFunctions($nTest,$aFunctionNames, $aTestDetail, $nFailedCount, $aNodes);

        if($nFailedCount > 0)
        {
            drupal_set_message(t('Total '.$sTestTitle.' failed tests = ' . $nFailedCount . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'),'error');
        } else {
            drupal_set_message(t('All ' . $nTest . ' '.$sTestTitle.' tests succeeded!' . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'));
        }
        return $nFailedCount;
        
    }    

    private static function runStandardCompilerTestsForManyFunctions(&$nTest, $aFunctionNames, &$aTestDetail, &$nFailedCount, &$aNodes)
    {
        $var_map = array();
        $var_map['MYVAR1'] = 55;
        $var_map['MYVAR2'] = 111;
        $var_map['MYVAR3'] = 333;
        $var_map['MYVAR99'] = NULL;

        $expression = '';
        foreach($aFunctionNames as $sFunctionName)
        {
            if($expression > '') 
            {
                $expression .= ' and '; 
            }
            $expression .= $sFunctionName . '(MYVAR1,MYVAR2,MYVAR3)';
        }
        
        $nTest++;
        $bExpectSytaxError=FALSE;
        $aExpectedVars = array('MYVAR1','MYVAR2','MYVAR3');
        TestUtility::runOneRuleCompilerTest($nTest, $bExpectSytaxError, $aExpectedVars, $var_map, $expression, $aTestDetail, $nFailedCount, $aNodes);
    }
    
    private static function runStandardCompilerTestsForOneFunction(&$nTest, $sFunctionName, &$aTestDetail, &$nFailedCount, &$aNodes)
    {
        $var_map = array();
        $var_map['MYVAR1'] = 55;
        $var_map['MYVAR2'] = 111;
        $var_map['MYVAR3'] = 333;
        $var_map['MYVAR99'] = NULL;
        
        $nTest++;
        $bExpectSytaxError=FALSE;
        $aExpectedVars = array('MYVAR1');
        $expression = $sFunctionName.'(MYVAR1)';
        TestUtility::runOneRuleCompilerTest($nTest, $bExpectSytaxError, $aExpectedVars, $var_map, $expression, $aTestDetail, $nFailedCount, $aNodes);

        $nTest++;
        $bExpectSytaxError=FALSE;
        $aExpectedVars = array('MYVAR1','MYVAR2');
        $expression = $sFunctionName.'(MYVAR1 , MYVAR2)';
        TestUtility::runOneRuleCompilerTest($nTest, $bExpectSytaxError, $aExpectedVars, $var_map, $expression, $aTestDetail, $nFailedCount, $aNodes);

        $nTest++;
        $bExpectSytaxError=FALSE;
        $aExpectedVars = array('MYVAR1','MYVAR2','MYVAR3');
        $expression = $sFunctionName.'(MYVAR1 , MYVAR2 , MYVAR3)';
        TestUtility::runOneRuleCompilerTest($nTest, $bExpectSytaxError, $aExpectedVars, $var_map, $expression, $aTestDetail, $nFailedCount, $aNodes);
    }
    
}
