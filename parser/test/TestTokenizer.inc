<?php
/**
 * ------------------------------------------------------------------------------------
 * Created by SAN Business Consultants
 * Designed and implemented by Frank Font (ffont@sanbusinessconsultants.com)
 * In collaboration with Andrew Casertano (acasertano@sanbusinessconsultants.com)
 * Open source enhancements to this module are welcome!  Contact SAN to share updates.
 *
 * Copyright 2014 SAN Business Consultants, a Maryland USA company (sanbusinessconsultants.com)
 *
 * Licensed under the GNU General Public License, Version 2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ------------------------------------------------------------------------------------
 *
 * This is a simple decision support engine module for Drupal.
 */


namespace simplerulesengine;

require_once (dirname(__FILE__) . '/../MeasureExpressionParser.inc');

/**
 * Test the tokenizer
 *
 * @author Frank Font
 */
class TestTokenizer
{
    public static function runAllTests()
    {
        $failedCount = 0;
        $failedCount += TestTokenizer::runTokenizer1Tests();
        $failedCount += TestTokenizer::runTokenizerTextTests();
        $failedCount += TestTokenizer::runTokenizerArrayTests();
        $failedCount += TestTokenizer::runTokenizerParameterTests();
        return $failedCount;
    }    

    public static function getResultAnalysis($nTest,$nExpectedTokenCount,$tokens_array,&$nFailedCount)
    {
        $nActualCount = count($tokens_array);
        if($nExpectedTokenCount != $nActualCount)
        {
            $nFailedCount++;
            $sTestDetail = ('Failed Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . $nActualCount . ' contents=' . print_r($tokens_array,TRUE) );
        } else {
            $sTestDetail = ('Okay Test' . $nTest . ': expected ' . $nExpectedTokenCount . ' got ' . $nActualCount . ' contents=' . print_r($tokens_array,TRUE) );
        }
        return $sTestDetail;
    }
    
    public static function runTokenizerTextTests()
    {
        $sTestTitle = 'Tokenizer Text';
        $nTest = 0;
        $nFailedCount = 0;
        
        $nTest++;
        $nExpectedTokenCount = 9;
        $expression = '("hello" = "hello")';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);
        
        $nTest++;
        $nExpectedTokenCount = 9;
        $expression = '("hello" == "hello")';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);
        
        $nTest++;
        $nExpectedTokenCount = 9;
        $expression = '("hello" <> "hello")';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);
        
        $nTest++;
        $nExpectedTokenCount = 7;
        $expression = '(VAR1 = "hello")';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);

        $nTest++;
        $nExpectedTokenCount = 7;
        $expression = '("hello" = VAR1)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);

        if($nFailedCount > 0)
        {
            drupal_set_message(t('Total '.$sTestTitle.' failed tests = ' . $nFailedCount . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'),'error');
        } else {
            drupal_set_message(t('All ' . $nTest . ' '.$sTestTitle.' tests succeeded!' . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'));
        }
        return $nFailedCount;
    }    
    
    public static function runTokenizer1Tests()
    {
        $sTestTitle = 'Tokenizer1';
        $nTest = 0;
        $nFailedCount = 0;

        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(1 > 2)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);

        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(10 > 20)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);

        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(1 = 2)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);

        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(1 <> 2)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);

        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(1 <= 2)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);

        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(1 >= 2)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);

        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(VAR1 and VAR2)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);
        
        $nTest++;
        $nExpectedTokenCount = 9;
        $expression = '((VAR1 or VAR2) and VAR3)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);
        
        $nTest++;
        $nExpectedTokenCount = 5;
        $expression = '(VAR1 or VAR2)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);
        
        if($nFailedCount > 0)
        {
            drupal_set_message(t('Total '.$sTestTitle.' failed tests = ' . $nFailedCount . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'),'error');
        } else {
            drupal_set_message(t('All ' . $nTest . ' '.$sTestTitle.' tests succeeded!' . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'));
        }
        return $nFailedCount;
    }
    
    public static function runTokenizerArrayTests()
    {
        $sTestTitle = 'Tokenizer Array';
        $nTest = 0;
        $nFailedCount = 0;
        
        $nTest++;
        $nExpectedTokenCount = 18;
        $expression = 'items("a","b","c","e")';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);
        
        $nTest++;
        $nExpectedTokenCount = 18;
        $expression = 'items("abc","bad","cats","eals")';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);
        
        if($nFailedCount > 0)
        {
            drupal_set_message(t('Total '.$sTestTitle.' failed tests = ' . $nFailedCount . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'),'error');
        } else {
            drupal_set_message(t('All ' . $nTest . ' '.$sTestTitle.' tests succeeded!' . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'));
        }
        return $nFailedCount;
    }    
    
    public static function runTokenizerParameterTests()
    {
        $sTestTitle = 'Tokenizer Array';
        $nTest = 0;
        $nFailedCount = 0;
        
        $nTest++;
        $nExpectedTokenCount = 4;
        $expression = 'MyTestFunctionA(MyVar1)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);
        
        $nTest++;
        $nExpectedTokenCount = 6;
        $expression = 'MyTestFunctionB(MyVar,MyVar21)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);
        
        $nTest++;
        $nExpectedTokenCount = 6;
        $expression = 'MyTestFunctionC(MyVar , MyVar21)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);

        $nTest++;
        $nExpectedTokenCount = 8;
        $expression = 'MyTestFunctionD(MyVar1,MyVar2,MyVar3)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);
        
        $nTest++;
        $nExpectedTokenCount = 8;
        $expression = 'MyTestFunctionE(MyVar1 , MyVar2 , MyVar3)';
        $tokens_array = MeasureExpressionParser::getTokens($expression);
        $aTestDetail[] = TestTokenizer::getResultAnalysis($nTest,$nExpectedTokenCount, $tokens_array, $nFailedCount);

        if($nFailedCount > 0)
        {
            drupal_set_message(t('Total '.$sTestTitle.' failed tests = ' . $nFailedCount . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'),'error');
        } else {
            drupal_set_message(t('All ' . $nTest . ' '.$sTestTitle.' tests succeeded!' . '<ul>' . implode('<li>', $aTestDetail) . '</ul>'));
        }
        return $nFailedCount;
    }    
    
}
